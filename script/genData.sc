import $ivy.`com.github.javafaker:javafaker:0.16`

@

import scala.util.Random
import java.util.UUID.randomUUID

import com.github.javafaker.Faker
import upickle.{ default => pkl }

case class TreeItem(
  groupId: String,
  userId: String,
  firstName: String,
  lastName: String,
  group1: String,
  group2: String,
  group3: String,
  group4: String,
  children: Option[Seq[TreeItem]] = None,
  members: Option[Seq[TreeItem]] = None
)

object TreeItem {
  def user(g: TreeItem)(implicit faker: Faker): TreeItem = g.copy(
    groupId = "",
    userId = faker.internet.uuid,
    firstName = faker.name.firstName,
    lastName = faker.name.lastName,
    children = None,
    members = None
  )
  def group(
    paths: List[String],
    children: Option[Seq[TreeItem]] = None,
    members: Option[Seq[TreeItem]] = None
  ): TreeItem = {
    val List(g1, g2, g3, g4, _*) = paths.padTo(4, "")
    TreeItem(
      groupId = randomUUID.toString,
      userId = "",
      firstName = "",
      lastName = "",
      group1 = g1,
      group2 = g2,
      group3 = g3,
      group4 = g4,
      children = children,
      members = members
    )
  }

  def groupCount(g: TreeItem): Int = {
    if (g.group4 != "") 4
    else if (g.group3 != "") 3
    else if (g.group2 != "") 2
    else 1
  }
}

implicit val userReadWrite: pkl.ReadWriter[TreeItem] =
  pkl.macroRW[TreeItem]

implicit val f = new Faker
val r = new Random

def normalizeGroupName(groupNames: List[String]) =
  groupNames.map(_.replaceAll("[ /]", "-").toLowerCase())

def genGroup1(): String = r.nextInt(3) match {
  case 0 => f.educator.campus
  case 1 => f.space.star
  case _ => f.address.streetName
}

def genGroupEx(): String = r.nextInt(8) match {
  case 0 => f.color.name
  case 1 => f.book.genre
  case 2 => f.esports.league
  case 3 => f.space.planet
  case 4 => f.weather.description
  case 5 => f.internet.domainName
  case 6 => f.food.spice
  case _ => f.team.creature
}

def genGroupSegments(base: String): List[String] = {
  (base :: Nil) ++ (0 to 2).collect {
    case _ if r.nextBoolean() => genGroupEx()
  }
}

def genGroup(size: Int): List[TreeItem] =
  Iterator.continually(genGroup1())
    .map(v => genGroupSegments(v))
    .map(normalizeGroupName)
    .take(size)
    .flatMap(subgroups)
    .map(paths => TreeItem.group(paths))
    .toList

def subgroups(xs: List[String]): List[List[String]] =
  xs.foldLeft(Nil: List[List[String]])((coll, that) => coll :+ (coll.lastOption match {
    case None       => that :: Nil
    case Some(last) => last :+ that
  }))

def assignMembers(xs: List[TreeItem]) = {
  xs.map { g =>
    val m = (0 to (r.nextInt(8) + 1)).map(_ => TreeItem.user(g))
    g.copy(members = Some(m))
  }
}

def buildTree(root: List[TreeItem]) = {
  ???
}

@main def main(i: Int = 0, p: String = ""): Unit = {
  val gs = genGroup(if (i < 1) 2 + r.nextInt(4) else i)
  val gm = assignMembers(gs)
  println {
    if (p != "")
      pkl.write(gm, indent = 2)
    else
      pkl.write(gm)
  }
}
